package edu.udmercy.horoscope

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SignAdapter(private val mainActivity: MainActivity, private val signList: List<Sign>): RecyclerView.Adapter<SignAdapter.ListItemHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemHolder
    {

        val itemView = LayoutInflater.from(parent.context).
            inflate(R.layout.listitem, parent, false)
        return ListItemHolder(itemView)
    }

    override fun getItemCount(): Int
    {
        if (signList != null){
            return signList.size
        }
        return -1
    }

    override fun onBindViewHolder(holder: ListItemHolder, position: Int)
    {
        val sign = signList[position]
        holder.title.text = sign.title
    }

    inner class ListItemHolder(view: View):
        RecyclerView.ViewHolder(view),
        View.OnClickListener {
      internal var title = view.findViewById<View>(R.id.textViewTitle) as TextView
        init
        {
            view.isClickable = true
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
           mainActivity.showSign(adapterPosition, title.text.toString())
        }
    }
}