package edu.udmercy.horoscope

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private var signList = ArrayList<Sign>()
    private var recyclerView: RecyclerView? = null
    private var adapter: SignAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById<View>(R.id.recyclerView) as RecyclerView
        adapter = SignAdapter(this, signList)
        val layoutManager = LinearLayoutManager(applicationContext)

        recyclerView!!.layoutManager = layoutManager

        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        recyclerView!!.adapter = adapter
        createNewSign("Aries", "Courageous and Energetic.", "Ram","April")
        createNewSign("Taurus", "Known for being reliable, practical, ambitious and sensual.", "Bull", "May")
        createNewSign("Gemini","Gemini-born are clever and intellectual.", "Twins", "June")
        createNewSign("Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July")
        createNewSign("Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "Lion",  "August")
        createNewSign("Virgo", "Methodical, meticulous, analytical and mentally astute.", "Virgin", "September")
        createNewSign("Libra","Librans are famous for maintaining balance and harmony.",  "Scales","October")
        createNewSign("Scorpio", "Strong willed and mysterious.","Scorpion","November")
        createNewSign("Sagittarius","Born adventurers.", "Archer", "December")
        createNewSign("Capricorn","The most determined sign in the Zodiac.",  "Goat", "January")
        createNewSign("Aquarius", "Humanitarians to the core", "Water Bearer", "February")
        createNewSign("Pisces", "Proverbial dreamers of the Zodiac.","Fish", "March")

    }

    fun createNewSign(str:String, desc:String, symbol:String, month:String){
        val s = Sign(str,desc,symbol,month)
        signList.add(s)
        adapter!!.notifyDataSetChanged()
    }
   fun showSign(signToShow: Int, text:String) {
       val myIntent = Intent(this, SignAPI::class.java)
       myIntent.putExtra("SNAME", text)
       Log.d("main", "created intent")
       startActivity(myIntent)
    }

}
