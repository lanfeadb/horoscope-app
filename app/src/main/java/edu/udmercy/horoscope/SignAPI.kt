package edu.udmercy.horoscope

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class SignAPI : AppCompatActivity() {
    var aries = Sign("Aries", "Courageous and Energetic.", "Ram","April")
    var taurus = Sign("Taurus", "Known for being reliable, practical, ambitious and sensual.", "Bull", "May")
    var gemini = Sign("Gemini","Gemini-born are clever and intellectual.", "Twins", "June")
    var cancer = Sign("Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July")
    var leo = Sign("Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "Lion",  "August")
    var virgo = Sign("Virgo", "Methodical, meticulous, analytical and mentally astute.", "Virgin", "September")
    var libra = Sign("Libra","Librans are famous for maintaining balance and harmony.",  "Scales","October")
    var scorpio = Sign("Scorpio", "Strong willed and mysterious.","Scorpion","November")
    var sagittarius = Sign("Sagittarius","Born adventurers.", "Archer", "December")
    var capricorn = Sign("Capricorn","The most determined sign in the Zodiac.",  "Goat", "January")
    var aquarius = Sign("Aquarius", "Humanitarians to the core", "Water Bearer", "February")
    var pisces = Sign("Pisces", "Proverbial dreamers of the Zodiac.","Fish", "March")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signdisplay)

        var sName = findViewById<TextView>(R.id.Name)
        var sDesc = findViewById<TextView>(R.id.desc)
        var sSymbol =findViewById<TextView>(R.id.symb)
        var sMonth = findViewById<TextView>(R.id.month)
        var sScope = findViewById<TextView>(R.id.scope)

        Log.d("API","before horoscope")
        val myIntent = Intent()
        val signTitle = intent.extras?.getString("SNAME")


        getHoroscope(signTitle, sScope)
        Log.d("API","after horoscope")
        when(signTitle?.toLowerCase())
        {
            "aries" -> display(aries, sName, sDesc, sSymbol,sMonth,sScope)
            "taurus"-> display(taurus, sName, sDesc, sSymbol,sMonth,sScope)
            "gemini"-> display(gemini, sName, sDesc, sSymbol,sMonth,sScope)
            "cancer"-> display(cancer, sName, sDesc, sSymbol,sMonth,sScope)
            "leo"-> display(leo, sName, sDesc, sSymbol,sMonth,sScope)
            "virgo"-> display(virgo, sName, sDesc, sSymbol,sMonth,sScope)
            "libra"-> display(libra, sName, sDesc, sSymbol,sMonth,sScope)
            "scorpio"-> display(scorpio, sName, sDesc, sSymbol,sMonth,sScope)
            "sagittarius"-> display(sagittarius, sName, sDesc, sSymbol,sMonth,sScope)
            "capricorn"-> display(capricorn, sName, sDesc, sSymbol,sMonth,sScope)
            "aquarius"-> display(aquarius, sName, sDesc, sSymbol,sMonth,sScope)
            "pisces"-> display(pisces, sName, sDesc, sSymbol,sMonth,sScope)
        }
    }


    fun display(sign:Sign, name:TextView, desc:TextView, symbol:TextView, month:TextView, scope:TextView)
    {
        name.text = sign.title
        desc.text = sign.description
        symbol.text = sign.symbol
        month.text = sign.month
        scope.text = sign.scope
    }

    fun getHoroscope(text:String?, textview:TextView) {
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)
        val url: String = "http://sandipbgt.com/theastrologer/api/horoscope/${text?.toLowerCase()}/today"
        // Request a string response from the provided URL.
        val stringReq = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                var strResp = response.toString()
                val jsonObj: JSONObject = JSONObject(strResp)
                var dailyHoroscope = jsonObj.getString("horoscope")
                textview!!.text = " $dailyHoroscope "
            },
            Response.ErrorListener { textview!!.text = "That didn't work!" })
        queue.add(stringReq)
    }
}
